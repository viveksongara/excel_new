<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessAndPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_and__personals', function (Blueprint $table) {
            $table->increments('business_id');
            $table->integer('user_id')->default('1');
            $table->string('months');
            $table->string('reporting_source');
            $table->string('debt_coverage_ratio');
            $table->string('net_worth_ratio');
            $table->string('current_ratio');
            $table->string('quick_ratio'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_and__personals');
    }
}
