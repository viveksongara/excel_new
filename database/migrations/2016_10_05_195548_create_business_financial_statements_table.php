<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessFinancialStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_financial_statements', function (Blueprint $table) {
            $table->increments('business_id');
            $table->integer('user_id')->default('1');
            $table->string('business');
            $table->string('debt_to_income_ratio');
            
            $table->date('caivrs_date');
            $table->string('caivrs_finding');
            $table->date('ofac_date');
            $table->string('ofac_finding');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_financial_statements');
    }
}
