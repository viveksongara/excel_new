<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskRateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_rate_scores', function (Blueprint $table) {
            $table->increments('risk_id');
            $table->integer('user_id')->default('1');
            $table->string('business_experience');
            $table->string('nature_of_business');
            $table->string('business_debt_service_coverage');
            $table->string('personal_debt_to_income');
            $table->string('management_experience');
            $table->string('personal_credit_quality');
            $table->string('collateral_coverage');
            $table->string('loan_score');
            $table->string('loan_grade');
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_rate_scores');
    }
}
