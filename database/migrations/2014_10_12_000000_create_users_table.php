<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('date');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->integer('role_id')->default(1);
            $table->integer('activation_status');
            $table->string('profile_picture');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('company');
            $table->string('confirmation_code');
            $table->string('country');
            $table->string('business_address');
            $table->string('business_name');
            $table->string('website_address');
            $table->string('referal_source');
            $table->string('job_creation');
                    
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
