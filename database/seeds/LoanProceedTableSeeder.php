<?php

use Illuminate\Database\Seeder;

class LoanProceedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    

        DB::table('proceeds')->insert([
            'user_id' => '1',
            'rbac' => 'null',
            'borrow' =>'null',
            'use'=>'null',
            'total'=>'null',
            
           
        ]);

        DB::table('proceeds')->insert([
            'user_id' => '2',
            'rbac' => 'null',
            'borrow' =>'null',
            'use'=>'null',
            'total'=>'null',
            
           
        ]);

        DB::table('proceeds')->insert([
            'user_id' => '3',
            'rbac' => 'null',
            'borrow' =>'null',
            'use'=>'null',
            'total'=>'null',
            
           
        ]);
    }
}
