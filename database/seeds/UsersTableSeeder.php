<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'name' => 'priyank',
            'email' => 'priyank086@gmail.com',
            'password' => bcrypt('1234567'),
            'phone' =>'11111111' ,
            'role_id' => '2',
           
        ]);
       DB::table('users')->insert([
            'name' => 'rahul kumar',
            'email' => 'rahul@gmail.com',
            'password' => bcrypt('12345'),
            'phone' =>'11111111' ,
            'role_id' => '1',
            'business_name' =>'suryaweb',
            'website_address' =>'suryaweb.com',
            'country' =>'india',
            'business_address' =>'indor',
            'referal_source' =>'mr rk',
            'job_creation' =>'book store',
        ]);
              DB::table('users')->insert([
            'name' => 'rahul kumar',
            'email' => 'rahul1212@gmail.com',
            'password' => bcrypt('12345'),
            'phone' =>'11111111' ,
            'role_id' => '1',
            'business_name' =>'suryaweb',
            'website_address' =>'suryaweb.com',
            'country' =>'india',
            'business_address' =>'indor',
            'referal_source' =>'mr rk',
            'job_creation' =>'book store',
        ]);
                     DB::table('users')->insert([
            'name' => 'rahul kumar',
            'email' => 'rahul121@gmail.com',
            'password' => bcrypt('12345'),
            'phone' =>'11111111' ,
            'role_id' => '1',
            'business_name' =>'suryaweb',
            'website_address' =>'suryaweb.com',
            'country' =>'india',
            'business_address' =>'indor',
            'referal_source' =>'mr rk',
            'job_creation' =>'book store',
        ]);

    }
}
