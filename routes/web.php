<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('export','Frontend@excel_test');

Route::post('user-registration','Frontend@user_registration');

Route::get('logout','Frontend@logout');

Route::get('user-registration-success',function(){
       return view('frontend/registration-success'); 
});

Route::get('confirm-user','Frontend@confirm_user');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin','middleware' => ['auth','role:2']], function () {
    Route::get('/','Admin@index');
    Route::get('profile/{user_id}','Admin@profile');
    Route::post('update-profile','Admin@update_profile');
    Route::get('user_list','Admin@user_list');
    Route::get('add_user',function(){
        return view('admin/add_user');
    });
    Route::post('user_added','Admin@add_usered');
    Route::post('add_loan_amount','Admin@add_loan_amount');
    Route::post('user_proceed','Admin@user_proceed');
     Route::get('statments_form',function(){
        return view('admin/statments_form');
    });
    Route::post('add_business_and_Personal','Admin@add_business_and_Personal');
    Route::post('add_business_financial_statement','Admin@add_business_financial_statement');
    Route::post('add_personal_financial_statement','Admin@add_personal_financial_statement');
     Route::get('risk_rate_score',function(){
        return view('admin/risk_rate_score');
    });
    Route::post('add_risk_rate_score','Admin@add_risk_rate_score');
    
    
    
    
});


