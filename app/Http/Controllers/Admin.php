<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\loan_amount;

use App\loan_purpose;

use App\proceed;

use App\business_and_Personal;
use App\business_financial_statement;
use App\personal_financial_statement;
use App\risk_rate_scores;
use Illuminate\Support\Facades\Input;

class Admin extends Controller
{
    public function index() {
        
       

        return view('admin.index');
        
    }
    
    public function blank() {
        
        return view('admin.blank');
        
    }
    
    public function profile($user_id) {  
        
        $data['users'] = User::find($user_id)->toArray();

        return view('admin.profile',$data);
        
    }
    
    public function update_profile(Request $request) {
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            
        ]);
        
        $update = array();
        $update['name'] = $request->name;
        $update['phone'] = $request->phone;
        if ($request->password != '') {
            $update['password'] = bcrypt($request->password);
        }        
        
        
        if (Input::hasFile('profile_picture')) {
            $file = Input::file('profile_picture');
            $file = $file->move(public_path() . '/assets/pics/profile_picture', rand(10000, 99999) . time() . '.' . $file->getClientOriginalExtension());
            $name = explode('/', $file->getRealPath());
            $update['profile_picture'] = end($name);
            
        }
        
        User::where('id','=',$request->user()->id)
              ->update($update);
        
        return redirect()->back()->with('message','Profile is successfully updated');
        
    }


// user list and loan amount show

    public function User_list(Request $request)
   {
       // $id=$request->user()->id;
       $data1['data1'] = user::select('*')
                ->where('role_id',1)
                ->get()
            ->toArray();

       $data['data'] =loan_amount::        
                 join('proceeds','loan_amounts.user_id','=','proceeds.user_id')
                 ->join('loan_purposes','proceeds.user_id','=','loan_purposes.user_id')
                 
                ->get()
                ->toArray();
        //print_r($data);
  return view('admin/user_list',$data1)->with($data);
   
   }
    
  // add user
   
   public function add_usered(Request $request)
   {
        $this->validate($request, 
         [
             
        'date' => 'required',
        'name' => 'required|  ',
         'email'=>'required|unique:user',    
        'business_name' => 'required',
        'business_address' => 'required',
        'country' => 'required',
         'referal_source' => 'required',
        'phone' => 'required',
         'job_creation' => 'required',
         ]);
       $User =new user;
       
       $User->date=$request->date;
       $User->name=$request->name;
       $User->email=$request->email;
       $User->business_name=$request->business_name;
       $User->business_address=$request->business_address;
       $User->country=$request->country;
       $User->referal_source=$request->referal_source;
       $User->phone=$request->phone;
       $User->job_creation=$request->job_creation;
       $User->save();
       $id=$User->id;
       return redirect()->back()->with('message','Data Submit Sucessfully')->with('id',$id);
   }

   public function add_loan_amount(Request $request)
   {
        $this->validate($request, 
         [      
        'loan_amount' => 'required|  ',
        'rate' => 'required',
        'loan_purpose' => 'required',
         'duration'=>'required',   
         ]);
        
        $Amount =new loan_amount;
       
       //$User->date=$request->date;
       $Amount->loan_amount=$request->loan_amount;
       $Amount->user_id=$request->user_id;
       $Amount->duration=$request->duration;
       $Amount->rate=$request->rate;
       $Amount->save();
       $id=$Amount->user_id;
       
        $Purpose =new loan_purpose;
        $Purpose->user_id=$id;
        $Purpose->Purpose=$request->loan_purpose;
        $Amount->save();
        return redirect()->back()->with('message','Data Submit Sucessfully')->with('id',$id);
   }
   
   public function user_proceed(Request $request)
   {
        $this->validate($request, 
         [      
        'use' => 'required|  ',
        'rbac' => 'required',
        'borrow' => 'required',
        
         ]);
        
        $Proceed =new \App\proceeds();
        $Proceed->user_id=$request->user_id;
        $Proceed->use=$request->use;
        $Proceed->rbac=$request->rbac;
        $Proceed->borrow=$request->borrow;
        $Proceed->save();
        return redirect()->back()->with('message','Data Submit Sucessfully')->with('id',$id);
   }
  // 	business form ki value insert krva na
   
   public function add_business_and_Personal(Request $request)
   {
       
               $this->validate($request, 
         [      
        'month' => 'required|  ',
        'reporting_source' => 'required',
        'debt_coverage_ratio' => 'required',
        'net_worth_ratio'=>'required',
        'current_ratio'=>'required',
        'quick_ratio'=>'required',
         ]);
        
        $Business =new business_and_Personal;
        $Business->user_id=1;
        $Business->months=$request->month;
        $Business->reporting_source=$request->reporting_source;
        $Business->debt_coverage_ratio=$request->debt_coverage_ratio;
        $Business->net_worth_ratio=$request->net_worth_ratio;
        $Business->current_ratio=$request->current_ratio;
        $Business->quick_ratio=$request->quick_ratio;
        $Business->save();
        return redirect()->back()->with('message','Data Submit Sucessfully');
        
   }

   public function add_business_financial_statement(Request $request)
   {
       
               $this->validate($request, 
         [      
        'business' => 'required|  ',
        'debt_to_income_ratio' => 'required',
        'caivrs_date' => 'required',
        'caivrs_finding'=>'required',
        'ofac_date'=>'required',
        'ofac_finding'=>'required',
         ]);
        
        $Business =new business_financial_statement;
        $Business->user_id=1;
        $Business->business=$request->business;
        $Business->debt_to_income_ratio=$request->debt_to_income_ratio;
        $Business->caivrs_date=$request->caivrs_date;
        $Business->caivrs_finding=$request->caivrs_finding;
        $Business->ofac_date=$request->ofac_date;
        $Business->ofac_finding=$request->ofac_finding;
        $Business->save();
        return redirect()->back()->with('message','Data Submit Sucessfully');
        
   }
   
   
   
   public function add_personal_financial_statement(Request $request)
   {
       
               $this->validate($request, 
         [      
        'partner_name' => 'required|  ',
        'debt_to_income_ratio' => 'required',
        'caivrs_date' => 'required',
        'caivrs_finding'=>'required',
        'ofac_date'=>'required',
        'ofac_finding'=>'required',
         ]);
        
        $Personal =new personal_financial_statement;
        $Personal->user_id=1;
        $Personal->partner_name=$request->partner_name;
        $Personal->debt_to_income_ratio=$request->debt_to_income_ratio;
        $Personal->credit_score=$request->credit_score;
        $Personal->caivrs_date=$request->caivrs_date;
        $Personal->caivrs_finding=$request->caivrs_finding;
        $Personal->ofac_date=$request->ofac_date;
        $Personal->ofac_finding=$request->ofac_finding;
        $Personal->save();
        return redirect()->back()->with('message','Data Submit Sucessfully');
        
   }
   
   public function add_risk_rate_score(Request $request)
   {
       
               $this->validate($request, 
         [      
        'business_experience' => 'required|  ',
        'nature_of_business' => 'required',
        'business_debt_service_coverage' => 'required',
        'personal_debt_to_income'=>'required',
        'management_experience'=>'required',
        'personal_credit_quality'=>'required',
        'collateral_coverage'=>'required',
        'loan_score'=>'required',
         'loan_grade'=>'required',
         ]);
        
        $Risk =new \App\risk_rate_score();
        $Risk->user_id=1;
        $Risk->business_experience=$request->business_experience;
        $Risk->nature_of_business=$request->nature_of_business;
        $Risk->business_debt_service_coverage=$request->business_debt_service_coverage;
        $Risk->personal_debt_to_income=$request->personal_debt_to_income;
        $Risk->management_experience=$request->management_experience;
        $Risk->personal_credit_quality=$request->personal_credit_quality;
        $Risk->collateral_coverage=$request->collateral_coverage;
        $Risk->loan_score=$request->loan_score;
        $Risk->loan_grade=$request->loan_grade;
        $Risk->save();
        return redirect()->back()->with('message','Data Submit Sucessfully');
        
   }
   
   
   }
   

