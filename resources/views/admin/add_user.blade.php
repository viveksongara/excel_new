@extends('layouts.admin.master')

@section('mainContent')
<div class="fluid-container">
<div class="col-md-12 col-sm-12">
    
     
 <div class="col-sm-4 col-md-4">
     <!--Error  check code start here---->
           @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <!--Error  check code ends here---->
            
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
                
      <form method='post' enctype="multipart/form-data" action="{{url('admin/user_added')}}">
          {{csrf_field()}}
         
                <h4>General Information	</h4>
                <div class="form-group">
                    <label for="input" class="">Date:</label>
                    <input  name="date"value="{{old('date')}}" type="date" class="form-control" id="date">
                </div>
                <div class="form-group">
                    <label for="input" class="">Client Name:</label>
                    <input  name="name"value="{{old('name')}}" type="text" class="form-control" id="name" placeholder="Enetr Name">
                </div>
                <div class="form-group">
                  <label for="input" class="">Business Name:</label>
                  <input  name="business_name"value="{{old('business_name')}}" type="text" class="form-control" id="business_name" placeholder="Enetr Business Name">
                </div>
               <div class="form-group">
                  <label for="input" class="">Email Address:</label>
                  <input  name="email"value="{{old('email')}}" type="text" class="form-control" id="email" placeholder="Enetr Email Address">
               </div> 
               <div class="form-group">
                  <label for="input" class="">Phone Number:</label>
                  <input  name="phone"value="{{old('phone')}}" type="text" class="form-control" id="phone" placeholder="Enetr Phone Number">
               </div> 
                <div class="form-group">
                  <label for="input" class="">Business Address:</label>
                  <input  name="business_address"value="{{old('business_address')}}" type="text" class="form-control" id="business_address" placeholder="Enetr Business Address">
                </div>
                <div class="form-group">
                  <label for="input" class="">County:</label>
                  <input  name="country"value="{{old('country')}}" type="country" class="form-control" id="country" placeholder="Enetr Country">
                </div>
               
               <div class="form-group">
                  <label for="input" class="">Referal Source:</label>
                  <input  name="referal_source"value="{{old('referal_source')}}" type="text" class="form-control" id="referal_source" placeholder="Enetr Referal Source">
               </div>
               <div class="form-group">
                  <label for="input" class="">Job Creation:</label>
                  <input  name="job_creation"value="{{old('job_creation')}}" type="text" class="form-control" id="job_creation" placeholder="Enetr Job Creation">
               </div> 
          
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
                </form>
       </div>
    <div class="col-sm-4 col-md-4">
 <form method='post' enctype="multipart/form-data" action="{{url('admin/add_loan_amount')}}">
          {{csrf_field()}}
         
                <h4>Loan Information </h4>
                 @if(Session::has('message'))
                 <input type="text" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
               
                <div class="form-group">
                    <label for="input" class="">Loan Amount</label>
                    <input  name="loan_amount" value="{{old('loan_amount')}}" type="text" class="form-control" id="name" placeholder="Enetr Loan Amount">
                </div>
                <div class="form-group">
                 <label for="input" class="">Duration in Month</label>
                    <input  name="duration" value="{{old('duration')}}" type="text" class="form-control" id="duration" placeholder="Enetr Duration in Month">
                </div>
          
                <div class="form-group">
                  <label for="input" class="">Interest Rate:</label>
                     <select class="form-control" id="rate" name="rate">
                         <option value="6.25">SBA CA (6.25%)</option>
                        <option value="6">SBA Micro (6%) </option>
                        <option value="6" >RBAC Capital (6%)</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="input" class="">Loan Purpose:</label>
                  <textarea id="loan_purpose" name="loan_purpose" class="form-control" cols="5" rows="5" value="{{old('loan_purpose')}}" ></textarea>
                </div>
                
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
           </form>
      
     </div>
  <div class="col-sm-4 col-md-4">
 <form method='post' enctype="multipart/form-data" action="{{url('admin/user_proceed')}}">
          {{csrf_field()}}
         
                <h4>Use of Proceeds:</h4>
                
                 @if(Session::has('message'))
                 <input type="hidden" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
                <div class="form-group">
                    <label for="input" class="">Use:</label>
                    <input  name="use"value="{{old('use')}}" type="text" class="form-control" id="use">
                </div>
                <div class="form-group">
                    <label for="input" class="">RBAC:</label>
                    <input  name="rbac"value="{{old('rbac')}}" type="rbac" class="form-control" id="rbac" placeholder="Enetr RBAC">
                </div>
                <div class="form-group">
                  <label for="input" class="">Borrower:</label>
                  <input  name="borrow"value="{{old('borrow')}}" type="text" class="form-control" id="borrow" placeholder="Enetr Borrower">
                </div>
            
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
            </form>  
            
       </div>
     
</div>
    
</div>
@stop