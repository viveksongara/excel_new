@extends('layouts.admin.master')

@section('mainContent')
<div class="fluid-container">
    <!--Error  check code start here---->
           @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <!--Error  check code ends here---->
            
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
           <!--form for Business and Personal-->
<div class="col-md-12 col-sm-12">
    
     
 <div class="col-sm-6 col-md-6">
     
             <h4>Risk Rate Score</h4>
        
              <form method='post' enctype="multipart/form-data" action="{{url('admin/add_risk_rate_score')}}">
                     {{csrf_field()}}
                
                 @if(Session::has('message'))
                 <input type="hidden" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
          
         
                <div class="form-group">
                    <label for="input" class="">Business Experience:</label>
                    <input  name="business_experience" value="{{old('business_experience')}}" type="text" class="form-control" id="business_experience" placeholder="Enetr Business Experience">
                </div>
                <div class="form-group">
                  <label for="input" class="">Nature of Business:</label>
                  <input  name="nature_of_business"value="{{old('nature_of_business')}}" type="text" class="form-control" id="borrow" placeholder="Enetr Nature of Business">
                 </div>
                 <div class="form-group">
                  <label for="input" class="">Business Debt Service Coverage:</label>
                  <input  name="business_debt_service_coverage" value="{{old('business_debt_service_coverage')}}" type="text" class="form-control" id="business_debt_service_coverage" placeholder="Enetr Business Debt Service Coverage">
                 </div>
                <div class="form-group">
                  <label for="input" class="">Personal Debt to Income</label>
                  <input  name="personal_debt_to_income" value="{{old('personal_debt_to_income')}}" type="text" class="form-control" id="personal_debt_to_income" placeholder="Enetr Personal Debt to Income">
                </div>
                
                <div class="form-group">
                  <label for="input" class="">Management Experience</label>
                  <input  name="management_experience" value="{{old('management_experience')}}" type="text" class="form-control" id="management_experience" placeholder="Enetr Management Eexperience">
                </div>
                <div class="form-group">
                  <label for="input" class="">Personal Credit Quality</label>
                  <input  name="personal_credit_quality" value="{{old('personal_credit_quality')}}" type="text" class="form-control" id="personal_credit_quality" placeholder="Enetr Personal Credit Quality">
                </div>
                <div class="form-group">
                  <label for="input" class="">Collateral Coverage</label>
                  <input  name="collateral_coverage" value="{{old('collateral_coverage')}}" type="text" class="form-control" id="collateral_coverage" placeholder="Enetr Personal Collateral Coverage">
                </div>
                <div class="form-group">
                  <label for="input" class="">Loan Score</label>
                  <input  name="loan_score" value="{{old('loan_score')}}" type="text" class="form-control" id="loan_score" placeholder="Enetr Loan Score">
                </div>
                <div class="form-group">
                  <label for="input" class="">Loan Grade:</label>
                  <input  name="loan_grade" value="{{old('loan_grade')}}" type="text" class="form-control" id="loan_grade" placeholder="Enetr Loan Grade">
                </div>
           
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
            </form>
                
      
       </div>
     
</div>
    
</div>
@stop