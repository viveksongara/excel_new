@extends('layouts.admin.master')

@section('mainContent')
<div class="fluid-container">
    <!--Error  check code start here---->
           @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <!--Error  check code ends here---->
            
            @if(Session::has('message'))
                <div class='alert alert-success'>
                    {{Session::get('message')}}
                </div>
                @endif
           <!--form for Business and Personal-->
<div class="col-md-12 col-sm-12">
    
     
 <div class="col-sm-4 col-md-4">
     
             <h4>Business and Personal</h4>
        
              <form method='post' enctype="multipart/form-data" action="{{url('admin/add_business_and_Personal')}}">
                     {{csrf_field()}}
                
                 @if(Session::has('message'))
                 <input type="hidden" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
          
                <div class="form-group">
                  <label for="input" class="">Months:</label>
                     <select class="form-control" id="month" name="month">
                        <option value="1">1</option>
                        <option value="2">2 </option>
                        <option value="3" >3</option>
                        <option value="4" >4</option>
                        <option value="5" >5</option>
                        <option value="6" >6</option>
                        <option value="7" >7</option>
                        <option value="8" >8</option>
                        <option value="9" >9</option>
                        <option value="10" >10</option>
                        <option value="11" >11</option>
                        <option value="12" >12</option>
                  </select>
                </div>     
                <div class="form-group">
                    <label for="input" class="">Reporting Source:</label>
                    <input  name="reporting_source"value="{{old('reporting_source')}}" type="text" class="form-control" id="reporting_source" placeholder="Enetr Reporting Source">
                </div>
                <div class="form-group">
                  <label for="input" class="">Debt Coverage Ratio:</label>
                  <input  name="debt_coverage_ratio"value="{{old('debt_coverage_ratio')}}" type="text" class="form-control" id="borrow" placeholder="Enetr Debt Coverage Ratio">
                 </div>
                 <div class="form-group">
                  <label for="input" class="">Debt/Net Worth Ratio:</label>
                  <input  name="net_worth_ratio"value="{{old('net_worth_ratio')}}" type="text" class="form-control" id="net_worth_ratio" placeholder="Enetr Net Worth_Ratio">
                 </div>
                <div class="form-group">
                  <label for="input" class="">Net Working Capital</label>
                  <input  name="net_working_capital"value="{{old('net_working_capital')}}" type="text" class="form-control" id="net_working_capital" placeholder="Enetr Net Working Capital">
                </div>
                
                <div class="form-group">
                  <label for="input" class="">Current Ratio</label>
                  <input  name="current_ratio" value="{{old('current_ratio')}}" type="text" class="form-control" id="current_ratio" placeholder="Enetr Current Ratio">
                </div>
                <div class="form-group">
                  <label for="input" class="">Quick Ratio</label>
                  <input  name="quick_ratio" value="{{old('quick_ratio')}}" type="text" class="form-control" id="quick_ratio" placeholder="Enetr Quick Ratio">
                </div>

           
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
            </form>
                
      
       </div>
    <div class="col-sm-4 col-md-4">
        <!-- form for Personal Financial Statement-->
               <form method='post' enctype="multipart/form-data" action="{{url('admin/add_personal_financial_statement')}}">
          {{csrf_field()}}
         
                <h4>Personal Financial Statement:</h4>
                
                 @if(Session::has('message'))
                 <input type="hidden" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
                <div class="form-group">
                    <label for="input" class="">Partner Name:</label>
                    <input  name="partner_name"value="{{old('partner_name')}}" type="text" class="form-control" id="partner_name" placeholder="Enetr Partner Name">
                </div>
                <div class="form-group">
                    <label for="input" class="">Debt to Income Ratio:</label>
                    <input  name="debt_to_income_ratio"value="{{old('debt_to_income_ratio')}}" type="text" class="form-control" id="debt_to_income_ratio" placeholder="Enetr Debt to Income Ratio">
                </div>
                <div class="form-group">
                  <label for="input" class="">Credit Score:</label>
                  <input  name="credit_score"value="{{old('credit_score')}}" type="text" class="form-control" id="credit_score" placeholder="Enetr Credit_Score">
                </div>
                <div class="form-group">
                  <label for="input" class="">Caivrs Date:</label>
                  <input  name="caivrs_date"value="{{old('caivrs_date')}}" type="date" class="form-control" id="caivrs_date" placeholder="Enetr Caivrs Date">
                </div>
                <div class="form-group">
                  <label for="input" class="">Caivrs Findings:</label>
                  <input  name="caivrs_finding"value="{{old('caivrs_finding')}}" type="text" class="form-control" id="caivrs_finding" placeholder="Enetr Caivrs Finding">
                </div>
                <div class="form-group">
                  <label for="input" class="">OFAC Date:</label>
                  <input  name="ofac_date"value="{{old('ofac_date')}}" type="date" class="form-control" id="ofac_date" placeholder="Enetr OFAC Date">
                </div>
                <div class="form-group">
                  <label for="input" class="">OFAC Findings:</label>
                  <input  name="ofac_finding"value="{{old('ofac_finding')}}" type="text" class="form-control" id="ofac_finding" placeholder="Enetr OFAC Finding">
                </div>
            
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
            </form>          
        <!--  Personal Financial Statement end -->
        
     </div>
  <div class="col-sm-4 col-md-4">
       <!-- form for Personal Financial Statement-->
               <form method='post' enctype="multipart/form-data" action="{{url('admin/add_business_financial_statement')}}">
          {{csrf_field()}}
         
                <h4>Business Financial Statement:</h4>
                
                 @if(Session::has('message'))
                 <input type="hidden" id="user_id" name="user_id" value="{{Session::get('id')}}"/>
                 
                @endif
                <div class="form-group">
                    <label for="input" class="">Business:</label>
                    <input  name="business" value="{{old('business')}}" type="text" class="form-control" id="business" placeholder="Enetr Business">
                </div>
                <div class="form-group">
                    <label for="input" class="">Debt to Income Ratio:</label>
                    <input  name="debt_to_income_ratio"value="{{old('debt_to_income_ratio')}}" type="text" class="form-control" id="debt_to_income_ratio" placeholder="Enetr Debt to Income Ratio">
                </div>
               
                <div class="form-group">
                  <label for="input" class="">Caivrs Date:</label>
                  <input  name="caivrs_date"value="{{old('caivrs_date')}}" type="date" class="form-control" id="caivrs_date" placeholder="Enetr Caivrs Date">
                </div>
                <div class="form-group">
                  <label for="input" class="">Caivrs Findings:</label>
                  <input  name="caivrs_finding"value="{{old('caivrs_finding')}}" type="text" class="form-control" id="caivrs_finding" placeholder="Enetr Caivrs Finding">
                </div>
                <div class="form-group">
                  <label for="input" class="">OFAC Date:</label>
                  <input  name="ofac_date"value="{{old('ofac_date')}}" type="date" class="form-control" id="ofac_date" placeholder="Enetr OFAC Date">
                </div>
                <div class="form-group">
                  <label for="input" class="">OFAC Findings:</label>
                  <input  name="ofac_finding"value="{{old('ofac_finding')}}" type="text" class="form-control" id="ofac_finding" placeholder="Enetr OFAC Finding">
                </div>
            
               <div class="form-group">
                  <input  name="" value="Submit" type="submit" class="btn btn-primary" id="sub"  >
              </div>
            </form>          
        <!--  Personal Financial Statement end -->
        
      
      
       </div>
     
</div>
    
</div>
@stop